<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asistencias extends CI_Controller {
  
    function __construct()
    {
    	
        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
            redirect('/');
        }
        $this->load->library('grocery_CRUD');

    }
	
    public function index($data = null)
    {

        $crud = new grocery_CRUD();
        $crud->set_table('tbl_asistencias');
        $crud->set_subject('Asistencias');
        $crud->set_relation('id_empleado','tbl_empleados','{cedula_empleado} - {nombre_empleado} {apellido_empleado}');
        $crud->display_as('id_empleado','Empleado');
        $crud->display_as('horae_asistencia','Entrada');
        $crud->display_as('horas_asistencia','Salida');
        $crud->display_as('fecha_asistencia','Fecha');
        $crud->unset_columns('fechac_asistencia','fecham_asistencia');
        $crud->unset_add();
        $crud->unset_edit();
        $salida = $crud->render();
        $this->load->view('header_admin',$salida);
        $this->load->view('asistencias_view',$salida);
        $this->load->view('footer_admin');

    }

}

