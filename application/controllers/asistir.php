<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asistir  extends CI_Controller {
  
    function __construct()
    {
      
        parent::__construct();
        $this->load->model("dst_model","dst");
        $this->load->library("form_validation");

    }

    public function index($data = null)
    {
      
      // $this->dst->set_table("tbl_cargos");
      $this->form_validation->set_rules("cedula", "Cédula", "required|max_length[11]|numeric");
   
      if ($this->form_validation->run() == FALSE) 
      {
        $this->load->view('header_pacceso');
        $this->load->view('pacceso',$data);
        $this->load->view('footer_pacceso');
      } 
      else 
      {
        $BuscarEmpleado = $this->dst;
        $BuscarEmpleado->set_table("tbl_empleados");
        $consultar = array(
                      'cedula_empleado' => $this->input->post('cedula') 
                    );
        $emp = $BuscarEmpleado->get($consultar);

        if ($emp) 
        {
          $YaAsistio  = $this->dst;
          $YaAsistio->set_table("tbl_asistencias");
          $consulta = array(
                         'id_empleado' => $emp[0]->id_empleado,
                    'fecha_asistencia' => date("Y-m-d")
                      );
          
          if (!$YaAsistio->get($consulta))
          {
            $asistencias = $this->dst;
            $asistencias->set_table("tbl_asistencias");
            $registro = array(
                          'id_empleado' => $emp[0]->id_empleado,
                          'horae_asistencia' => date("Y-m-d H:i:s"),
                          'fecha_asistencia' => date("Y-m-d"),
                          'fechac_asistencia' => date("Y-m-d H:i:s"),
                        );
            
            $asiste = $asistencias->insert($registro);
            if ($asiste) 
            {
              $data['mensaje']['tipo'] = 'success';
              $data['mensaje']['mensaje'] = "Trabajador identificado correctamente";
            }
            else
            {
              $data['mensaje']['tipo'] = 'error';
              $data['mensaje']['mensaje'] = "Ocurrió un error: el Trabajador no pudo ser ingresado";
            }
          }
          else
          {
            
            $consulta = array(
                         'id_empleado' => $emp[0]->id_empleado,
                    'fecha_asistencia' => date("Y-m-d")
                      );
            $registro = array(
                          'horas_asistencia' => date("Y-m-d H:i:s")
                        );

            if($YaAsistio->update($registro,$consulta))
            {
              $data['mensaje']['tipo'] = 'success';
              $data['mensaje']['mensaje'] = "Trabajador de Salida";
            }

          }

        }  
        else  
        {
          $data['mensaje']['tipo'] = 'error';
          $data['mensaje']['mensaje'] = "El trabajador no se encuentra registrado";
        }
        
      }
      
      $this->load->view('header_pacceso');
      $this->load->view('pacceso',$data);
      $this->load->view('footer_pacceso');

    }

}
