<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cargos extends CI_Controller {
  
    function __construct()
    {
    	
        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
            redirect('/');
        }
        $this->load->library('grocery_CRUD');

    }
	
    public function index($data = null)
    {

        $crud = new grocery_CRUD();
        $crud->set_table('tbl_cargos');
        $crud->set_subject('Cargo');
        $crud->unset_fields('fechac_cargo','fecham_cargo');
        $crud->unset_columns('fechac_cargo','fecham_cargo');
        $crud->display_as('nombre_cargo', 'Cargo');
        $crud->display_as('cod_cargo', 'Código de Cargo');
        $crud->display_as('tipo_cargo', 'Tipo de Cargo');
        $crud->display_as('nivel_cargo', 'Nivel de Cargo');
        $crud->display_as('sueldo_cargo', 'Sueldo');
        
        $crud->order_by('nombre_cargo','ASC');
        $crud->set_rules('nombre_cargo', 'Cargo',"required|alpha_space|max_length[100]|min_length[2]");
        $crud->set_rules('tipo_cargo', 'Tipo de Cargo',"required");
        $crud->set_rules('sueldo_cargo', 'Sueldo',"required|decimal");
        $salida = $crud->render();
        $this->load->view('header_admin',$salida);
        $this->load->view('cargos_view',$salida);
        $this->load->view('footer_admin');

    }

}

