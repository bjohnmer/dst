<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departamentos extends CI_Controller {
  
    function __construct()
    {
    	
        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
            redirect('/');
        }
        $this->load->library('grocery_CRUD');

    }
	
    public function index($data = null)
    {

    $crud = new grocery_CRUD();
    $crud->set_table('tbl_departamentos');
    $crud->set_subject('Departamento');
    $crud->unset_fields('fechac_departamento','fecham_departamento');
    $crud->unset_columns('fechac_departamento','fecham_departamento');
    $crud->display_as('nombre_departamento','Departamento');
    $crud->order_by('nombre_departamento','ASC');
    $crud->set_rules('nombre_departamento', 'Departamento',"required|alpha_space|max_length[100]|min_length[2]");
    $salida = $crud->render();
    $this->load->view('header_admin',$salida);
    $this->load->view('departamentos_view',$salida);
    $this->load->view('footer_admin');

    }

}

