<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dpersonales extends CI_Controller {
  
  function __construct()
	{
		
    parent::__construct();

    $this->load->library('session');
    if (!$this->session->userdata("logged_in")){
      redirect(base_url().'welcome');
    }
  }
	
	public function index($data = null)
	{
  
    $this->load->view('header_admin');
    $this->load->view('dpersonales',$data);
    $this->load->view('footer_admin');
    
	}

}

