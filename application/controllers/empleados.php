<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empleados extends CI_Controller {
  
    function __construct()
    {
    	
        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
            redirect('/');
        }
        $this->load->library('grocery_CRUD');

    }
	
    public function index($data = null)
    {

        $crud = new grocery_CRUD();
        $crud->set_table('tbl_empleados');
        $crud->set_subject('Empleado');
        $crud->unset_fields('id_empleado','fechac_empleado','fecham_empleado');
        $crud->unset_columns('email_empleado','fechac_empleado','fecham_empleado');
        $crud->set_relation('id_cargo','tbl_cargos','{nombre_cargo} {tipo_cargo}');
        $crud->set_relation('id_departamento','tbl_departamentos','{nombre_departamento}');
        $crud->columns('codigo_empleado','cedula_empleado','nombre_empleado','apellido_empleado','telf_empleado','fechai_empleado','id_cargo','id_departamento','tipo_empleado');
        $crud->display_as('codigo_empleado','Código');
        $crud->display_as('cedula_empleado','Cédula');
        $crud->display_as('nombre_empleado','Nombre');
        $crud->display_as('apellido_empleado','Apellido');
        $crud->display_as('telf_empleado','Teléfono');
        $crud->display_as('fechai_empleado','Fecha de Ingreso');
        $crud->display_as('id_cargo','Cargo');
        $crud->display_as('id_departamento','Departamento');
        $crud->display_as('tipo_empleado','Tipo');

        $salida = $crud->render();

        $this->load->view('header_admin',$salida);
        $this->load->view('empleados_view',$salida);
        $this->load->view('footer_admin');

    }

}

