<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
  
  function __construct()
	{
		
    parent::__construct();
		$this->load->model("login_model");
    
    $this->load->library("form_validation");
    $this->load->library("session");
    $this->load->library('encrypt');
  
  }
  
  public function entrar()
  {

    // Validación de los campos
    $this->form_validation->set_rules("login_usuario", "Usuario", "required|max_length[20]");
    $this->form_validation->set_rules("password_usuario", "Clave", "required|max_length[50]");

    // si la Validación dió error
    if ($this->form_validation->run() == FALSE) {
      // se devuelve para el inicio
      $this->load->view('header');
      $this->load->view('inicial');
      $this->load->view('footer');
    } 
    else 
    {
      //$clave = $this->encrypt->decode(set_value("password_usuario"));
      $clave = md5(set_value("password_usuario"));
      $data = $this->login_model->check_login(set_value("login_usuario"), $clave);
      if ($data) {

        $this->session->set_userdata("logged_in" , TRUE);
        
        $this->session->set_userdata("id_usuario",$data->id_usuario);
        $this->session->set_userdata("id_empleado",$data->id_empleado);
        $this->session->set_userdata("cedula_empleado",$data->cedula_empleado);
        $this->session->set_userdata("nombre_empleado",$data->nombre_empleado);
        $this->session->set_userdata("apellido_empleado",$data->apellido_empleado);
        $this->session->set_userdata("login_usuario",$data->login_usuario);

        redirect('admin/');

      } else  {
        $data['logged_in_fail'] = TRUE;
        $data['mensaje'] = "Nombre de usuario o clave inválida";
        $this->load->view('header');
        $this->load->view('inicial',$data);
        $this->load->view('footer');
      }
    }
    
	}

  public function logout($data = null){
    $this->session->set_userdata("logged_in", FALSE);
    $this->session->sess_destroy();
    $this->load->view('header');
    $this->load->view('inicial',$data);
    $this->load->view('footer');
  }


}

