<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nomina extends CI_Controller {
  
    function __construct()
    {
    	
        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
            redirect('/');
        }
        $this->load->model("dst_model","dst");
        $this->load->model("nomina_model","nomina");
        $this->load->library('grocery_CRUD');
        $this->load->library('form_validation');
        $this->load->library('datemanager');

    }

    public function index($data = null, $view = 'bnomina_view')
    {
        $this->load->view('header_admin',$data);
        $this->load->view($view,$data);
        $this->load->view('footer_admin');
    }

    public function verNomina()
    {
        $this->form_validation->set_rules("desde", "Desde", "required");
        $this->form_validation->set_rules("hasta", "Hasta", "required");
        $dtNomina = null;
        if ($this->form_validation->run() == FALSE) 
        {
            $this->index();
        } 
        else
        {

            $n = new $this->nomina;
            $dtNomina['nomina'] = $n->getNomina(array(
                'horae_asistencia >=' => $this->datemanager->date2mySQL($this->input->post('desde')).' 00:00:00',
                'horas_asistencia <=' => $this->datemanager->date2mySQL($this->input->post('hasta')).' 23:59:59',
            ));
            $params = new $this->dst;
            $params->set_table('tbl_parametros');
            $dtNomina['parametros'] = $params->get(null,'id_parametro','DESC',1);
            /*
                echo "<pre>";
                print_r($dtNomina);
                echo "</pre>";
            */
            
        }

        $dtNomina['desde'] = $this->input->post('desde');
        $dtNomina['hasta'] = $this->input->post('hasta');

        $this->printNomina($dtNomina);

    }

    public function printNomina($data = null, $view = 'pnomina_view')
    {
        $this->load->view('header_print',$data);
        $this->load->view($view,$data);
        $this->load->view('footer_print');
    }

    public function parametros()
    {
        $this->load->library('grocery_CRUD');
        $crud = new grocery_CRUD();
        $crud->set_table('tbl_parametros');
        $crud->set_subject('Parametros');
    
        $crud->display_as('horasdiarias_parametro','Jornada de Trabajo(hrs/dia)');
        $crud->display_as('cestaticketdia_parametro','Cestatickets(unidades/dia)');
        $crud->display_as('sso_parametro','S.S.O.');
        $crud->display_as('pf_parametro','Paro Forzoso');
        $crud->display_as('fj_parametro','Fondo de Jubilación');
        $crud->display_as('fvh_parametro','F.V.H.');
        $crud->display_as('bnf_parametro','Bono Nocturno Fijo');
    
        $crud->set_rules('horasdiarias_parametro','Jornada de Trabajo(hrs/dia)','required|numeric');
        $crud->set_rules('cestaticketdia_parametro','Cestatickets(unidades/dia)','required|numeric');
        $crud->set_rules('sso_parametro','S.S.O.','required|numeric');
        $crud->set_rules('pf_parametro','Paro Forzoso','required|numeric');
        $crud->set_rules('fj_parametro','Fondo de Jubilación','required|numeric');
        $crud->set_rules('fvh_parametro','F.V.H.','required|numeric');
        $crud->set_rules('bnf_parametro','Bono Nocturno Fijo','required|numeric');
        
        $crud->unset_list();
        $crud->unset_back_to_list();
        $crud->unset_add();
        $crud->unset_delete();

        $salida = $crud->render();
        $this->load->view('header_admin',$salida);
        $this->load->view('asistencias_view',$salida);
        $this->load->view('footer_admin');
    }


}

