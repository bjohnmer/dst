<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pacceso extends CI_Controller {
  
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }
	
	public function index($data = null)
	{
  
        $this->load->view('header_pacceso');
        $this->load->view('pacceso',$data);
        $this->load->view('footer_pacceso');
    
	}

}

