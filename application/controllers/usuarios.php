<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    function __construct()
    {

        parent::__construct();

        $this->load->library('session');
        if (!$this->session->userdata("logged_in")){
            redirect('/');
        }
        $this->load->library('grocery_CRUD');

    }

    public function index($data = null)
    {

        $crud = new grocery_CRUD();
        $crud->set_table('tbl_usuarios');
        $crud->set_subject('Usuario');
        $crud->unset_columns('password_usuario','fechac_usuario','fecham_usuario');
        $crud->unset_fields('id_usuario','fechac_usuario','fecham_usuario');
        $crud->columns('id_empleado','login_usuario');
        $crud->display_as('id_empleado','Empleado');
        $crud->display_as('login_usuario','Login');
        $crud->set_relation('id_empleado','tbl_empleados','{cedula_empleado} - {nombre_empleado} {apellido_empleado}');

        $crud->callback_before_insert(array($this,'encrypt_pwd_callback'));
        $crud->callback_before_update(array($this,'encrypt_pwd_callback'));

        $crud->callback_edit_field('password_usuario',array($this,'vaciar_campo_pwd'));

        $salida = $crud->render();
        $this->load->view('header_admin',$salida);
        $this->load->view('usuarios_view',$salida);
        $this->load->view('footer_admin');

    }

    function encrypt_pwd_callback($post_array, $primary_key = null)
    {
        if (!empty($post_array['password_usuario']))
        {
            $post_array['password_usuario'] = md5($post_array['password_usuario']);
        }
        else
        {
            unset($post_array['password_usuario']);
        }
        return $post_array;
    }

    function vaciar_campo_pwd($post_array, $primary_key = null)
    {
        return '<input type="password" maxlength="50" name="password_usuario" id="field-password_usuario">';
    }

}

