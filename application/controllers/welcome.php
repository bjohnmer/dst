<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
  
  function __construct()
	{
		parent::__construct();
		
	}
	
	public function index($vista = "inicial", $data = null)
  {
    $this->load->view('header');
    $this->load->view($vista,$data);
    $this->load->view('footer');
  }

  public function dst()
	{
    $this->index('dst_view');
	}

  public function filosofia()
  {
    $this->index('filosofia_view');
  }
  
  public function organizacion()
  {
    $this->index('organizacion_view');
  }
  public function RRHH()
  {
    $this->index('RRHH_view');
  }
  public function contactos()
  {
    $this->index('contactos_view');
  }

}

