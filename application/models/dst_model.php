<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dst_model extends CI_Model {
	protected $tabla;
	function __construct(){
		parent::__construct();
	}
	
	public function set_table($tbl = null)
	{
		if ($this->db->table_exists($tbl))
		{
			$this->tabla = $tbl;
      return true;
		}
    return false;
	}
	
	public function get($data = array(),$order = null, $torder = null, $limit = null, $group = null)
	{
		if (!empty($data)) 
		{
  		$this->db->where($data);
  		if (!empty($order) && !empty($torder)) 
  		{
  			$this->db->order_by($order, $torder);	
  		}
  		if (!empty($limit))
  		{
  			$this->db->limit($limit);		
  		}
      if (!empty($group))
      {
        $this->db->group_by($group);   
      }
  		$rst = $this->db->get($this->tabla);
  	}
  	else
    {
    	if (!empty($order) && !empty($torder)) 
    	{
      	$this->db->order_by($order, $torder);
    	}
    	if (!empty($limit))
  		{
  			$this->db->limit($limit);		
  		}
      if (!empty($group))
      {
        $this->db->group_by($group);   
      }
      $rst = $this->db->get($this->tabla);
    }

    if ($rst->num_rows()) 
    {
      return $rst->result();      
    } 
    return false;
  }

	public function insert($data = array())
  {
  	if (!empty($data))
  	{
  		$this->db->insert($this->tabla, $data);
  		return true;
  	}
  	return false;
  }

  public function multi_insert($data = array())
  {
  	if (!empty($data))
  	{
  		$this->db->insert_batch($this->tabla, $data);
  		return true;
  	}
    return false;
  }
  public function update($data = null, $query = null)
  {
  	if (!empty($data) && !empty($query)) {
  		if($this->db->update($this->tabla, $data, $query))
  		{
  			return true;
  		}
			return false;
  	}
  }

	/*public function check_login($usuario, $clave)
	{

		$data = $this->db->where("login_usuario", $usuario);
		$data = $this->db->where("password_usuario", $clave);
		$data = $this->db->join('tbl_empleados', 'tbl_empleados.id_empleado = tbl_usuarios.id_usuario');
		$data = $this->db->get("tbl_usuarios");

		if ($data->num_rows()) {
			return $data->row(0);
		} else {
			return false;
		}

	}*/
}