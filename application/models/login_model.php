<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	public function check_login($usuario, $clave)
	{

		$data = $this->db->where("login_usuario", $usuario);
		$data = $this->db->where("password_usuario", $clave);
		$data = $this->db->join('tbl_empleados', 'tbl_empleados.id_empleado = tbl_usuarios.id_usuario');
		$data = $this->db->get("tbl_usuarios");

		if ($data->num_rows()) {
			return $data->row(0);
		} else {
			return false;			
		}

	}
}
