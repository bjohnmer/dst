<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nomina_model extends CI_Model {
	protected $tabla;
	function __construct(){
		parent::__construct();
    $this->set_table();
	}
	
	public function set_table($tbl = "tbl_asistencias")
	{
		if ($this->db->table_exists($tbl))
		{
			$this->tabla = $tbl;
      return true;
		}
    return false;
	}
	
	public function getNomina($data = array(),$order = null, $torder = null, $limit = null, $group = 'id_empleado')
	{
    $this->db->select('*, SUM(TIMESTAMPDIFF(MINUTE, horae_asistencia , horas_asistencia )/60) as horas ');
    if (!empty($data)) 
    {
     $this->db->where($data);
    }
    $this->db->group_by('a.'.$group);
    
    if (!empty($order) && !empty($torder)) 
    {
      $this->db->order_by($order, $torder); 
    }
    
    if (!empty($limit))
    {
      $this->db->limit($limit);   
    }

    $this->db->join('tbl_empleados', 'tbl_empleados.id_empleado = a.id_empleado');
    // $this->db->join('tbl_cargos', 'tbl_cargos.id_cargo = tbl_empleado.id_cargo');
    
    $rst = $this->db->get($this->tabla ." as a ");
    
    if ($rst->num_rows()) 
    {
      return $rst->result();      
    } 
    return false;

  }

}