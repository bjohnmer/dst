
    <div class="row-fluid">
      <div class="span10">
        <div class="span12">
          <h2>Departamento de Recursos Humanos (RRHH)</h2>
          <p>Las funciones que se desarrollan en el departamento de recursos humanos  comprenden las actividades relacionadas con la planificación de la plantilla, selección y formación del personal. La misma requiere una serie de tareas administrativas, como:</p>
          <ul>
            <li>Elección y formalización de los contratos.</li>
            <li>Gestión de nominas y seguros sociales.</li>
            <li>Gestión de permisos, vacaciones, horas extraordinarias, bajas por enfermedad.</li>
            <li>Control de absentismo.</li>
            <li>Régimen disciplinario.</li>
          </ul>

          <p>Otra  función es la de retribución que consiste en el estudio de formulas salariales, la política de incentivos y el establecimiento de niveles salariales de las diferentes categorías profesionales como:</p>
          <ul>
            <li>Personal Médico.</li>
            <li>Personal Asistencial, técnico y Paramédico.</li>
            <li>Personal Administrativo, cada uno con diferentes responsabilidades y roles dentro de las instituciones de salud pero todos con un mismo objetivo que es  brindar el mejor servicio en la atención de pacientes, preocupación que debe considerarse como la prioridad de la organización de salud.   </li>
          </ul>
        </div>
      </div>