<div class="row-fluid">
  <div class="span12">
    <h3>Rango de Nómina</h3>
    <form class="bs-docs-example form-horizontal" action="<?=base_url()?>nomina/verNomina" method="post">
      <div class="control-group">
        <label for="desde" class="control-label">Desde:</label>
        <div class="controls">
          <input type="text" placeholder="Desde" name="desde" id="desde" value="<?=date('d/m/Y')?>" readonly>
        </div>
      </div>
      <div class="control-group">
        <label for="hasta" class="control-label">Hasta:</label>
        <div class="controls">
          <input type="text" placeholder="Hasta" name="hasta" id="hasta" value="<?=date('d/m/Y')?>" readonly>
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <button class="btn" type="submit">Buscar</button>
        </div>
      </div>
      <?php if (validation_errors()): ?>
        <div class="alert alert-error">
          <?=validation_errors()?>
        </div>
      <?php endif ?>
      <?php if (isset($mensaje['tipo'])): ?>
        <div class="alert alert-<?=$mensaje['tipo']?>">
          <?=$mensaje['mensaje']?>
        </div>
      <?php endif ?>    
    </form>
  </div>
</div>