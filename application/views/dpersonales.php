<div class="row-fluid">
  <div class="span5">
    <div class="span4"></div>
    <div class="span8">
      <h3>Datos Personales</h3>
    </div>
    <form class="bs-docs-example form-horizontal" action="<?=base_url()?>">
      <div class="control-group">
        <label for="nombre_usuario" class="control-label">Nombre</label>
        <div class="controls">
          <input name="nombre_usuario" type="text" placeholder="Nombre" id="nombre_usuario" value="<?=$this->session->userdata('nombre_empleado')." ".$this->session->userdata('apellido_empleado')?>" disabled>
        </div>
      </div>
      <div class="control-group">
        <label for="login_usuario" class="control-label">Login</label>
        <div class="controls">
          <input name="login_usuario" type="text" placeholder="Login" id="login_usuario" value="<?=$this->session->userdata('login_usuario')?>">
        </div>
      </div>
      <div class="control-group">
        <label for="password_usuario" class="control-label">Clave</label>
        <div class="controls">
          <input name="password_usuario" type="password" placeholder="Clave" id="password_usuario">
        </div>
      </div>
      <div class="control-group">
        <label for="cpassword_usuario" class="control-label">Confirme su clave</label>
        <div class="controls">
          <input type="password" placeholder="Confirme su clave" id="cpassword_usuario">
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <button class="btn btn-success" type="submit">Guardar Datos</button>
        </div>
      </div>
    </form>
  </div>
</div>