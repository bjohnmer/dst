
    <div class="row-fluid">
      <div class="span10">
        <div class="span12">
          <h2>Filosfía de Gestión</h2>
          <h3>Objetivos:</h3>
          <ul>
            <li>Elaborar políticas en materia de salud integral, lo cual incluye promoción de la salud y calidad de vida, prevención, restitución de la salud  y rehabilitación.</li>
            <li>Diseñar, implantar y controlar la calidad de redes nacionales para el diagnóstico y vigilancia en salud pública.</li>
            <li>Coordinar programas, planes y acciones con otras instancias públicas y privadas que propicien un medio ambiente saludable y una población sana.</li>
            <li>Formular  y ejecutar políticas atinentes a la producción nacional de insumos, medicamentos y productos biológicos para la salud, en coordinación con el Ministerio del Poder Popular para las Industrias Ligeras y Comercio.</li>
          </ul>
          
          <h3>Misión:</h3>
          <p>Establecer las políticas del Sector Salud en el Municipio Trujillo, con el objeto de elevar la calidad de vida de la población, incorporando la comunidad y las instituciones vinculadas al sector en el proceso de toma de decisiones.</p>
          
          <h3>Visión:</h3>
          <p>Fundación autónoma con proyección Nacional e Internacional orientada al mejoramiento del nivel de vida de los trujillanos a través de un servicio integral de salud con capacidad gerencial para el manejo de los recursos (suficientes y oportunos), mediante la innovación de políticas que den respuesta coherentes y efectivas al colectivo en general bajo una visión autogestionaria que asegure la participación plena de la comunidad.</p>
          
        </div>
      </div>