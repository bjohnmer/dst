    <div id="push"></div>
  </div><!-- /container -->
  <div id="footer">
    <div class="container nv">
      <p class="muted credit">&copy; Distrito Sanitario  - Trujillo <a href="#">Nelson Valera</a> y <a href="#">Haydee Zambrano</a>.</p>
    </div>
  </div>

  <!-- Le javascript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="<?=base_url()?>js/jquery.js"></script>
  <script src="<?=base_url()?>js/bootstrap-dropdown.js"></script>
  <script src="<?=base_url()?>js/bootstrap-collapse.js"></script>
  <script src="<?=base_url()?>js/holder/holder.js"></script>


  <?php if (!empty($js_files)): ?>
    <?php foreach($js_files as $file): ?>
      <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
  <?php endif ?>


  <?php if (empty($js_files)): ?>
    <script src="<?=base_url()?>js/jquery.js"></script>
  <?php endif ?>
  <script src="<?=base_url()?>js/jquery-ui-1.10.3.custom.js"></script>
  <script>
    $( "#desde, #hasta" ).datepicker({
      inline: true,
      dateFormat : 'dd/mm/yy',
      maxDate: "0"
    });
  </script>
</body>
</html>