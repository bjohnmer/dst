<div id="push"></div>
  </div><!-- /container -->
  <div id="footer">
    <div class="container nv">
      <p class="muted credit">&copy; Distrito Sanitario  - Trujillo <a href="#">Nelson Valera</a> y <a href="#">Haydee Zambrano</a>.</p>
    </div>
  </div>

  <!-- Le javascript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="<?=base_url()?>js/jquery.js"></script>
  <script src="<?=base_url()?>js/clock.js"></script>

</body>
</html>