
  </div><!-- /container -->
 

  <!-- Le javascript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="<?=base_url()?>js/jquery.js"></script>
  <script src="<?=base_url()?>js/jquery-ui-1.10.3.custom.js"></script>
  <script>
    $(document).ready(function(){
      $('#impr').click(function(){
        $(this).hide();
        $('#atras').hide();
        window.print();
        $(this).show();
        $('#atras').show();
      });
      $('#atras').click(function(){
        window.location = "<?=base_url()?>nomina";
      });
    });
  </script>
</body>
</html>