<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Distrito Sanitario Trujillo</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="<?=base_url()?>css/bootstrap.css" rel="stylesheet">
  <link href="<?=base_url()?>css/bootstrap-responsive.css" rel="stylesheet">
  <link href="<?=base_url()?>css/personal.css" rel="stylesheet">
</head>
<body>
  <div class="container" id="wrap">
    <div class="row-fluid">

        <div class="span3">
          <img src="<?=base_url()?>img/logo1_mppps.jpeg" alt="">
        </div>
        <div class="span7">
          <div class="page-header">
            <h1>
              Registro y Control de Asistencias del Distrito Sanitario-Trujillo
            </h1>
          </div>
        </div>
        <div class="span2">
          <img src="<?=base_url()?>img/logo2_mppps.jpeg" alt="" class="hidden-phone">
        </div>
      </div>
      <div class="row-fluid">
        <div class="navbar">
            <div class="navbar-inner">
              <div class="container nv">
                
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </a>
                
                <div class="nav-collapse collapse">
                  <ul class="nav">
                    <li <?php if ($this->uri->segment(2) == "") :?>class="active"<?php endif; ?>><a href="<?=base_url()?>welcome">Inicio</a></li>
                    <li <?php if ($this->uri->segment(2) == "dst") :?>class="active"<?php endif; ?>><a href="<?=base_url()?>welcome/dst">Distrito Sanitario Trujillo</a></li>
                    <li <?php if ($this->uri->segment(2) == "filosofia") :?>class="active"<?php endif; ?>><a href="<?=base_url()?>welcome/filosofia">Filosofía de Gestión</a></li>
                    <li <?php if ($this->uri->segment(2) == "organizacion") :?>class="active"<?php endif; ?>><a href="<?=base_url()?>welcome/organizacion">Estructura Organizacional</a></li>
                    <li <?php if ($this->uri->segment(2) == "RRHH") :?>class="active"<?php endif; ?>><a href="<?=base_url()?>welcome/RRHH">RRHH</a></li>
                    <li <?php if ($this->uri->segment(2) == "contactos") :?>class="active"<?php endif; ?>><a href="<?=base_url()?>welcome/contactos">Contactos</a></li>
                  </ul>
                </div>

              </div>
            </div>
          </div>  
      </div>