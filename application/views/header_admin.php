<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Distrito Sanitario </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="<?=base_url()?>css/bootstrap.css" rel="stylesheet">
  <link href="<?=base_url()?>css/bootstrap-responsive.css" rel="stylesheet">
  <link href="<?=base_url()?>css/personal.css" rel="stylesheet">

  <?php if (!empty($css_files)): ?>
    <?php foreach($css_files as $file): ?>
      <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
  <?php else: ?>
    <link href="<?=base_url()?>css/blitzer/jquery-ui-1.10.3.custom.css" rel="stylesheet">
  <?php endif ?>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- <link href="<?=base_url()?>css/overcast/jquery-ui-1.10.3.custom.css" rel="stylesheet"> -->
</head>
<body>
  <div class="container" id="wrap">
    <div class="row-fluid">
      <div class="span3">
        <img src="<?=base_url()?>img/logo1_mppps.jpeg" alt="">
      </div>
      <div class="span7">
        <div class="page-header">
          <h1>
            Registro y Control de Asistencias del Distrito Sanitario-Trujillo
          </h1>
        </div>
      </div>
      <div class="span2">
        <img src="<?=base_url()?>img/logo2_mppps.jpeg" alt="" class="hidden-phone">
      </div>
    </div>
    <div class="row-fluid">
      <div class="navbar">
          <div class="navbar-inner">
            <div class="container nv">
              
              <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              
              <div class="nav-collapse collapse">
                <ul class="nav">
                  <li <?php if ($this->uri->segment(1) == "admin") :?>class="active"<?php endif; ?>><a href="<?=base_url()?>admin">Inicio</a></li>
                  <li <?php if ($this->uri->segment(1) == "departamentos") :?>class="active"<?php endif; ?> ><a href="<?=base_url()?>departamentos">Departamentos</a></li>
                  <li <?php if ($this->uri->segment(1) == "cargos") :?>class="active"<?php endif; ?> ><a href="<?=base_url()?>cargos">Cargos</a></li>
                  <li <?php if ($this->uri->segment(1) == "empleados") :?>class="active"<?php endif; ?> ><a href="<?=base_url()?>empleados">Empleados</a></li>
                  <li <?php if ($this->uri->segment(1) == "asistencias") :?>class="active"<?php endif; ?> ><a href="<?=base_url()?>asistencias">Asistencias</a></li>
                  <li <?php if ($this->uri->segment(1) == "usuarios") :?>class="active"<?php endif; ?> ><a href="<?=base_url()?>usuarios">Usuarios</a></li>
                </ul>
                <ul class="nav pull-right">
                  <li><a href="#"><?=$this->session->userdata('nombre_empleado')." ".$this->session->userdata('apellido_empleado')?></a></li>
                  <li class="divider-vertical"></li>
                  <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">Sesión <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="<?=base_url()?>usuarios/index/edit/<?=$this->session->userdata('id_empleado')?>">Datos Personales</a></li>
                      <!-- 
                      <li><a href="<?=base_url()?>dpersonales">Datos Personales</a></li>
                       -->
                      <li class="divider"></li>
                      <li><a href="<?=base_url()?>login/logout">Cerrar Sesión</a></li>
                    </ul>
                  </li>
                </ul>
              </div>

            </div>
          </div>
        </div>  
    </div>