
    <div class="row-fluid">
      <div class="span5">
        <div class="span4 hidden-phone hidden-tablet"></div>
        <div class="span9">
          <h3>Ingrese al sistema</h3>
        </div>
        
        <div class="span8">
          <form class="bs-docs-example form-horizontal" action="<?=base_url()?>login/entrar" method="post">
            <div class="control-group">
              <label for="inputUsuario" class="control-label">Usuario</label>
              <div class="controls">
                <input type="text" placeholder="Usuario" id="inputUsuario" name="login_usuario">
              </div>
            </div>
            <div class="control-group">
              <label for="inputClave" class="control-label">Clave</label>
              <div class="controls">
                <input type="password" placeholder="Clave" id="inputClave" name="password_usuario">
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <button class="btn" type="submit">Entrar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="span7 thumbnail">
        <img src="<?=base_url()?>img/salud.jpg">
      </div>