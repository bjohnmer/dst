
    <div class="row-fluid">
      <div class="span10">
        <div class="span12">
          <h2>Estructura Organizacional</h2>
          <p>
            <img src="<?=base_url()?>img/organigrama.png" alt="">
          </p>
          
        </div>
      </div>

      <!-- 
    google.load('visualization', '1', {packages:['orgchart']});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        data.addColumn('string', 'ToolTip');
        data.addRows([
          [{v:'funda', f:'Distrito Sanitario '}, null, null],
          [{v:'dd', f:'Direcci&oacute;n Distrital'}, 'funda', null],
          [{v:'ds',f:'Distrito Sanitario Trujillo'},'dd',null],
          [{v:'us',f:'Unidad Sanitaria Trujillo'},'dd',null],
          [{v:'sa', f:'Secci&oacute;n Ambulatoria'}, 'ds', null],
          [{v:'sh', f:'Secci&oacute;n Hospitalaria'}, 'ds', null],
          [{v:'smed', f:'Secci&oacute;n M&eacute;dica'}, 'us', null],
          [{v:'sam', f:'Secci&oacute;n Ambulatoria'}, 'us', null],
          [{v:'stec', f:'Secci&oacute;n T&eacute;cnica'}, 'us', null],
          [{v:'aur', f:'Ambulatorios Urbanos'}, 'sa', null],
          [{v:'abps', f:'Ambulatorios Bio-Psicosociales'}, 'sa', null],
          [{v:'ar2', f:'Ambulatorios Rurales 2'}, 'sa', null],
          [{v:'ar1', f:'Ambulatorios Rurales 1'}, 'sa', null],
          [{v:'h2', f:'Hospital II "J.G.H."'}, 'sh', null],
          [{v:'hE', f:'Hospital E "A.P.R."'}, 'sh', null],
          [{v:'urn', f:'Unidad de Recuperaci&oacute;n Nutricional'}, 'sh', null],
          
           
          [{v:'clin', f:'Cl&iacute;nica'}, 'smed', null],
          [{v:'cpre', f:'Consulta Preventiva'}, 'smed', null],
          [{v:'its', f:'ITS/SIDA'}, 'smed', null],
          [{v:'had', f:'Hig. Adulto'}, 'smed', null],
          [{v:'hinf', f:'Hig. Infantil'}, 'smed', null],
          [{v:'hesc', f:'Hig. Escolar'}, 'smed', null],
          [{v:'hmen', f:'Hig. Mental'}, 'smed', null],
  

          [{v:'ofta', f:'Oftalmolog&iacute;a'}, 'clin', null],
          [{v:'derma', f:'Dermatolog&iacute;a'}, 'clin', null],
          [{v:'derma2', f:'Dermatolog&iacute;a S.'}, 'clin', null],
          [{v:'cardio', f:'Cardiolog&iacute;a'}, 'clin', null],
          [{v:'neumo', f:'Neumonolog&iacute;a'}, 'clin', null],

        ]);
        var chart = new google.visualization.OrgChart(document.getElementById('visualization'));
        chart.draw(data, {allowHtml:true});
      }
       -->