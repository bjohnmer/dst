
      <div class="span3">
        <img src="<?=base_url()?>img/logo1_mppps.jpeg" alt="">
      </div>
      <div class="span7">
        <div class="page-header">
          <h1>
            Registro y Control de Asistencias del Distrito Sanitario-Trujillo
          </h1>
        </div>
      </div>
      <div class="span2">
        <img src="<?=base_url()?>img/logo2_mppps.jpeg" alt="" class="hidden-phone">
      </div>
    </div>
    <hr>
    <div class="row-fluid">
      <!-- <div class="span-10">
        
      </div>
 -->
      <div class="span5">
        <div class="span4 hidden-phone hidden-tablet"></div>
        <div class="span9">
          <h3>Bienvenido</h3>
        </div>
        
        <div class="span8">
          <h1>Punto de acceso</h1>
          <div class="clock">
            <div id="Date"></div>
            <ul>
              <li id="hours"> </li>
                <li id="point">:</li>
                <li id="min"> </li>
                <li id="point">:</li>
                <li id="sec"> </li>
            </ul>
          </div>
          <form class="bs-docs-example form-horizontal" action="<?=base_url()?>asistir" method="post">
            <div class="control-group">
              <div class="controls">
                <input tabindex="1" type="text" placeholder="Cédula" id="cedula" name="cedula">
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <button class="btn" type="submit">Entrar</button>
              </div>
            </div>
            <?php if (validation_errors()): ?>
              <div class="alert alert-error">
                <?=validation_errors()?>
              </div>
            <?php endif ?>
            <?php if (isset($mensaje['tipo'])): ?>
              <div class="alert alert-<?=$mensaje['tipo']?>">
                <?=$mensaje['mensaje']?>
              </div>
            <?php endif ?>
          </form>
        </div>
      </div>
      <div class="span7 thumbnail">
        <img src="<?=base_url()?>img/salud.jpg">
      </div>