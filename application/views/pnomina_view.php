<?php 
  $cargos = new $this->dst;
  $cargos->set_table('tbl_cargos');
        
  $departamentos = new $this->dst;
  $departamentos->set_table('tbl_departamentos');
/*
  echo "<pre>";
  print_r($parametros);
  echo "</pre>";
*/
?>
<table border="0" align="center" width="100%"  class="datos">
  <tr>
    <td><img src="<?=base_url()?>img/logo1_mppps.jpeg" alt=""></td>
    <td align="center">
      <h3>
        Registro y Control de Asistencias del Distrito Sanitario-Trujillo <br>
        Nómina de Empleados y Obreros
      </h3>
    </td>
    <td>
      <img src="<?=base_url()?>img/logo2_mppps.jpeg" width="150px" height="150px" alt="" class="hidden-phone">
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <strong>Desde: <?=$desde?> Hasta: <?=$hasta?> </strong>
      <button id="impr">Imprimir</button>
      <button id="atras">Atrás</button>
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <table border="1" align="center" width="100%"  class="datos">
        <thead>
          <tr>
            <th>
              CÓDIGO
            </th>
            <th>
              CÉDULA
            </th>
            <th>
              NOMBRE
            </th>
            <th>
              APELLIDO
            </th>
            <th>
              TIPO
            </th>
            <th>
              DEPARTAMENTO
            </th>
            <th>
              CARGO
              </th>
            <th>
              NIVEL
              </th>
            <th>
              SUELDO BASE
            </th>
            <th>
              SUELDO DIARIO
            </th>
            <th>
              DÍAS TRABAJADOS
            </th>
            <th>
              CESTATICKET
            </th>
            <th>
              S.S.O.
            </th>
            <th>
              PARO FORZOSO
            </th>
            <th>
              FONDO DE JUBILACION
            </th>
            <th>
              F.V.H
            </th>
            <th>
              BONO NOCTURNO FIJO
            </th>
            <th>
              SUELDO TOTAL
            </th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($nomina as $n): ?>
          <tr>
            <td>
            <?php if (!empty($n->codigo_empleado)): ?>
              <?=$n->codigo_empleado?>
            <?php else: ?>
              &nbsp;
            <?php endif ?>
            </td>
            <td>
            <?php if (!empty($n->cedula_empleado)): ?>
              <?=$n->cedula_empleado?>
            <?php else: ?>
              &nbsp;
            <?php endif ?>
            </td>
            <td>
            <?php if (!empty($n->nombre_empleado)): ?>
              <?=$n->nombre_empleado?>
            <?php else: ?>
              &nbsp;
            <?php endif ?>
            </td>
            <td>
            <?php if (!empty($n->apellido_empleado)): ?>
              <?=$n->apellido_empleado?>
            <?php else: ?>
              &nbsp;
            <?php endif ?>
            </td>
            <td>
            <!-- TIPO -->
            <?php if (!empty($n->tipo_empleado)): ?>
              <?=$n->tipo_empleado?>
            <?php else: ?>
              &nbsp;
            <?php endif ?>
            </td>
            <td>
              <!-- DEPARTAMENTO -->
              <?php
                $c = $cargos->get(array(
                  'id_cargo' => $n->id_cargo,                  
                  ));
                $d = $departamentos->get(array(
                  'id_departamento' => $n->id_departamento,                  
                  ));

              ?>
              <?php if (!empty($d[0]->nombre_departamento)): ?>
                <?=$d[0]->nombre_departamento?>
              <?php else: ?>
                &nbsp;
              <?php endif ?>
            </td>
            <td>
              <!-- CARGO -->
              <?php if (!empty($c[0]->nombre_cargo)): ?>
                <?=$c[0]->nombre_cargo?>
              <?php else: ?>
                &nbsp;
              <?php endif ?>
            </td>
            <td>
              <?php if (!empty($c[0]->tipo_cargo)): ?>
                <?=$c[0]->tipo_cargo?>
              <?php else: ?>
                &nbsp;
              <?php endif ?>
            </td>
            <td>
              <!-- SUELDO BASE -->
              <?php if (!empty($c[0]->sueldo_cargo)): ?>
                <?=$c[0]->sueldo_cargo?>
              <?php else: ?>
                &nbsp;
              <?php endif ?>
            </td>
            <td>
              <!-- SUELDO DIARIO -->
              <?php if (!empty($c[0]->sueldo_cargo)): ?>
                <?=$sd=round($c[0]->sueldo_cargo/30,2)?>
              <?php else: ?>
                &nbsp;
              <?php endif ?>
            </td>
            <td>
              <!-- DÍAS TRABAJADOS -->
              <?=$dt = round($n->horas/$parametros[0]->horasdiarias_parametro,2)?>
            </td>
            <td>
              <!-- CESTATICKET -->
              <?=$ct = $parametros[0]->cestaticketdia_parametro*ceil($dt)?>
            </td>
            <td>
              <!-- S.S.O. -->
              <!-- SUELDO BASE*12/52*4%*5 -->
              <?=$sso = round(($c[0]->sueldo_cargo*$parametros[0]->sso_parametro)/100,2)?>
            </td>
            <td>
              <!-- PARO FORZOSO -->
              <!-- SUELDO BASE*12/52*0,5%*5 -->
              <?=$sso = round(($c[0]->sueldo_cargo*$parametros[0]->pf_parametro)/100,2)?>
            </td>
            <td>
              <!-- FONDO DE JUBILACION -->
              <!-- SUELDO BASE *3% -->
              <?=$fj = round(($c[0]->sueldo_cargo*$parametros[0]->fj_parametro)/100,2)?>
            </td>
            <td>
              <!-- F.V.H -->
              <!-- SUELDO BASE *1% -->
              <?=$fvh = round(($c[0]->sueldo_cargo*$parametros[0]->fvh_parametro)/100,2)?>
            </td>
            <td>
              <!-- BONO NOCTURNO FIJO -->
              <!-- SUELDO BASE *50% -->
              <?=$bnf = round(($c[0]->sueldo_cargo*$parametros[0]->bnf_parametro)/100,2)?>
            </td>
            <td>
              <!-- SUELDO TOTAL -->
              <?=$st = round(($c[0]->sueldo_cargo + $bnf) - ($sso + $fj + $fvh),2)?>
            </td>
          </tr>
        <?php endforeach ?>
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <p class="muted credit">&copy; Distrito Sanitario  - Trujillo <a href="#">Nelson Valera</a> y <a href="#">Haydee Zambrano</a>.
    </td>
  </tr>
</table>