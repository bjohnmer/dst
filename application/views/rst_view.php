
    <div class="row-fluid">
      <div class="span10">
        <div class="span12">
          <h2>Distrito Sanitario Trujillo</h2>
          <p>
            El Distrito Sanitario Trujillo está ubicado en la Avenida Mendoza del Municipio Trujillos del Estado Trujillo, frente al Hospital “José Gregorio Hernández”.
          </p>
          <p>El territorio que ocupa el Estado Trujillo, caracterizado por su gran macizo montañoso de la Cordillera de los Ándes con su variado clima, fue cuna de la creación de la Medicatura de Sanidad para el año 1939, la cual se encontraba ubicada al final de la Avenida Bolívar con Calle Candelaria, cuyo fundador fue el Doctor Estanislao Núñez Carrillo, los servicios asistenciales que se les aportaban a la comunidad eran vacunaciones, control de la embrazada y del niño. Certificado de Salud y Venéreas, entre otros. Luego para el año 1942, la Unidad Sanitaria se Ubica en la Avenida Independencia con Calle Comercio, para le época su Director fue el Doctor Víctor M. Bocaranda González, prodigioso médico trujillano, estuvo en la Unidad Sanitaria durante 14 años, en su desempeño en ese cargo organizó y desarrolló programas y control de enfermedades venéreas, inspección sanitaria, fiebre amarilla, campaña antirrábica, certificado de salud, mortalidad infantil, trabajos epidemiológicos preventivos y curativos, campaña de vacunación, entre otros.</p>
          <p>Debido a la gran labor prestada por el Médico la Unidad Sanitaria de Trujillo a partir de Noviembre de 1994 recibe su nombre.</p>
          <p>El 9 de Octubre de 1957, se fundo el establecimiento donde actualmente funciona la Unidad Sanitaria de Trujillo, cuyo Director fue el Doctor Miseforo Rosales Albano, la misma presta a la comunidad Trujillana los Servicios Materno Infantil (Programa de Atención Materno Infantil), Epidemiología Distrital, Accidentes, Dermatología, Tuberculosis y el Departamento de Venéreas llamado a partir del 2000 Infecciones de Transmisión Sexual (ITS)</p>
          <p>La unidad Sanitaria Trujillo, es una organización que fue creada bajo la tutela del Ministerio de Sanidad y Asistencia Social, el cual fue creado el 10 de Marzo de 1936, desde su fundación actuó independientemente hasta que se crearon los Servicios Cooperativos (Dirección Regional de Salud), simultáneamente con el Distrito Sanitario Trujillo como una dependencia Técnico-Administrativo con siete Medicaturas Rurales, un Centro Materno Infantil y la Unidad Sanitaria.</p>
        </div>
      </div>