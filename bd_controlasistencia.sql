-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-10-2013 a las 21:24:02
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `bd_controlasistencia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_asistencias`
--

CREATE TABLE IF NOT EXISTS `tbl_asistencias` (
  `id_asistencia` int(11) NOT NULL AUTO_INCREMENT,
  `id_empleado` int(11) NOT NULL,
  `horae_asistencia` datetime NOT NULL,
  `horas_asistencia` datetime NOT NULL,
  `fecha_asistencia` date NOT NULL,
  `fechac_asistencia` datetime NOT NULL,
  `fecham_asistencia` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_asistencia`),
  KEY `id_empleado` (`id_empleado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `tbl_asistencias`
--

INSERT INTO `tbl_asistencias` (`id_asistencia`, `id_empleado`, `horae_asistencia`, `horas_asistencia`, `fecha_asistencia`, `fechac_asistencia`, `fecham_asistencia`) VALUES
(4, 1, '2013-07-17 08:10:27', '2013-07-17 15:40:45', '2013-07-17', '2013-07-17 15:10:27', '2013-07-22 16:26:14'),
(5, 2, '2013-07-17 15:13:04', '2013-07-17 15:13:31', '2013-07-17', '2013-07-17 15:13:04', '2013-07-17 19:43:31'),
(6, 1, '2013-07-24 14:12:26', '2013-07-24 18:12:42', '2013-07-24', '2013-07-24 14:12:26', '2013-07-24 18:43:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cargos`
--

CREATE TABLE IF NOT EXISTS `tbl_cargos` (
  `id_cargo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cargo` varchar(100) NOT NULL,
  `tipo_cargo` enum('PI','PII','PIII','TI','BI','BII','BIII','GENERAL','1','2','3','4','5','6','7','8','9','10') NOT NULL DEFAULT 'PI',
  `sueldo_cargo` double(9,2) NOT NULL,
  `fechac_cargo` datetime NOT NULL,
  `fecham_cargo` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_cargo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Volcado de datos para la tabla `tbl_cargos`
--

INSERT INTO `tbl_cargos` (`id_cargo`, `nombre_cargo`, `tipo_cargo`, `sueldo_cargo`, `fechac_cargo`, `fecham_cargo`) VALUES
(1, 'ANALISTA DE PERSONAL I', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(2, 'ANALISTA DE PERSONAL I', 'PII', 3297.05, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(3, 'ANALISTA DE PERSONAL I', 'PIII', 3325.50, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(4, 'ANALISTA DE PERSONAL II', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(5, 'ANALISTA DE PERSONAL II', 'PII', 3297.05, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(6, 'ANALISTA DE PERSONAL II', 'PIII', 3325.50, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(7, 'ANALISTA DE PERSONAL III', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(8, 'ANALISTA DE PERSONAL III', 'PII', 3297.05, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(9, 'ANALISTA DE PERSONAL III', 'PIII', 3325.50, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(10, 'ANALISTA DE PERSONAL IV', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(11, 'ANALISTA DE PERSONAL IV', 'PII', 3297.05, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(12, 'ANALISTA DE PERSONAL IV', 'PIII', 3325.50, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(13, 'ANALISTA DE PERSONAL V', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(14, 'ANALISTA DE PERSONAL V', 'PII', 3297.05, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(15, 'ANALISTA DE PERSONAL V', 'PIII', 3325.50, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(16, 'MÉDICO RURAL', 'GENERAL', 3462.80, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(17, 'MÉDICO I', 'GENERAL', 4620.00, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(18, 'ENFERMERA I', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(19, 'ENFERMERA I', 'PII', 3297.05, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(20, 'ENFERMERA I', 'PIII', 3325.50, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(21, 'ENFERMERA II', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(22, 'ENFERMERA II', 'PII', 3297.05, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(23, 'ENFERMERA II', 'PIII', 3325.50, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(24, 'ENFERMERA III', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(25, 'ENFERMERA III', 'PII', 3297.05, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(26, 'ENFERMERA III', 'PIII', 3325.50, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(27, 'ENFERMERA IV', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(28, 'ENFERMERA IV', 'PII', 3297.05, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(29, 'ENFERMERA IV', 'PIII', 3325.50, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(30, 'ENFERMERA V', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(31, 'ENFERMERA V', 'PII', 3297.05, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(32, 'ENFERMERA V', 'PIII', 3325.50, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(33, 'TÉCNICO EN TRABAJO SOCIAL', 'TI', 2988.29, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(34, 'TRABAJADOR SOCIAL', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(35, 'SECRETARIA I', 'BI', 2702.72, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(36, 'SECRETARIA I', 'BII', 2804.58, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(37, 'SECRETARIA I', 'BIII', 2888.04, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(38, 'SECRETARIA II', 'BI', 2702.72, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(39, 'SECRETARIA II', 'BII', 2804.58, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(40, 'SECRETARIA II', 'BIII', 2888.04, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(41, 'SECRETARIA III', 'BI', 2702.72, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(42, 'SECRETARIA III', 'BII', 2804.58, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(43, 'SECRETARIA III', 'BIII', 2888.04, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(44, 'SECRETARIA IV', 'BI', 2702.72, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(45, 'SECRETARIA IV', 'BII', 2804.58, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(46, 'SECRETARIA IV', 'BIII', 2888.04, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(47, 'HIGIENISTA DENTAL', 'BI', 2702.72, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(48, 'ODONTÓLOGO', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(49, 'AUXILIAR DE OFICINA', 'BI', 2702.72, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(50, 'TÉCNICO DE REGISTROS Y ESTADISTICA DE SALUD', 'TI', 2988.29, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(51, 'OPERADOR DE EQUIPO DE COMPUTACION', 'BI', 2702.72, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(52, 'CONTADOR', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(53, 'MÉDICO II', 'GENERAL', 4620.00, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(54, 'ADMINISTRADOR', 'PI', 3179.08, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(55, 'AUXILIAR DE ENFERMERÍA', '8', 2993.76, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(56, 'AYUDANTE DE SERVICIOS GENERALES', '3', 2786.52, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(57, 'CAMARERA', '1', 2702.72, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(58, 'VIGILANTE', '6', 2910.60, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(59, 'OPERADOR DE MÁQUINA DE IMPRESIÓN', '7', 2951.52, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(60, 'SUPERVISOR DE COCINA', '9', 3034.68, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(61, 'ELECTROMECÁNICO', '8', 2993.76, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(62, 'ASEADOR', '1', 2702.72, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(63, 'MENSAJERO', '3', 2786.52, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(64, 'AUXILIAR DE OFICINA', '5', 2869.68, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(65, 'AUXILIAR DE TERAPIA', '6', 2910.60, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(66, 'SUPERVISOR DE SERVICIOS INTERNOS', '9', 3034.68, '0000-00-00 00:00:00', '2013-07-22 22:04:25'),
(67, 'SUPERVISOR DE SERVICIOS ESPECIALES ', '10', 3075.60, '0000-00-00 00:00:00', '2013-07-22 22:04:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_departamentos`
--

CREATE TABLE IF NOT EXISTS `tbl_departamentos` (
  `id_departamento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_departamento` varchar(100) NOT NULL,
  `fechac_departamento` datetime NOT NULL,
  `fecham_departamento` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_departamento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `tbl_departamentos`
--

INSERT INTO `tbl_departamentos` (`id_departamento`, `nombre_departamento`, `fechac_departamento`, `fecham_departamento`) VALUES
(1, 'CONTABILIDAD', '0000-00-00 00:00:00', '2013-07-21 19:33:28'),
(2, 'DIRECCIÓN', '0000-00-00 00:00:00', '2013-07-21 19:37:46'),
(3, 'ADMINISTRACIÓN', '0000-00-00 00:00:00', '2013-07-21 19:37:46'),
(4, 'BIENES Y MATERIAS ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'RECURSOS HUMANOS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'ENFERMERÍA', '0000-00-00 00:00:00', '2013-07-21 19:37:46'),
(8, 'HIGIENE DE LOS ALIMENTOS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'EPIDEMIOLOGÍA', '0000-00-00 00:00:00', '2013-07-21 19:37:46'),
(10, 'PROYECTO CAREM ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'HIGIENE DEL ADULTO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'ITS/SIDA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'TUBERCULOSIS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'MATENIMIENTO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'DERMATOLOGÍA', '0000-00-00 00:00:00', '2013-07-21 19:37:46'),
(16, 'DEPÓSITO', '0000-00-00 00:00:00', '2013-07-21 19:37:46'),
(17, 'ODONTOLOGÍA', '0000-00-00 00:00:00', '2013-07-21 19:37:46'),
(18, 'TRIAJE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'MATERNO INFANTIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'ZOONOSIS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'NEUMONOLOGÍA', '0000-00-00 00:00:00', '2013-07-21 19:37:46'),
(22, 'VIGILANCIA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'TRABAJO SOCIAL', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_empleados`
--

CREATE TABLE IF NOT EXISTS `tbl_empleados` (
  `id_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `cedula_empleado` int(11) NOT NULL,
  `nombre_empleado` varchar(75) NOT NULL,
  `apellido_empleado` varchar(75) NOT NULL,
  `telf_empleado` varchar(30) DEFAULT NULL,
  `codigo_empleado` varchar(10) DEFAULT NULL,
  `email_empleado` varchar(50) DEFAULT NULL,
  `fechai_empleado` date DEFAULT NULL,
  `id_cargo` int(11) DEFAULT NULL,
  `id_departamento` int(11) DEFAULT NULL,
  `tipo_empleado` enum('Empleado','Obrero') NOT NULL DEFAULT 'Empleado',
  `fechac_empleado` datetime DEFAULT NULL,
  `fecham_empleado` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_empleado`),
  UNIQUE KEY `cedula_empleado` (`cedula_empleado`),
  KEY `id_cargo` (`id_cargo`,`id_departamento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tbl_empleados`
--

INSERT INTO `tbl_empleados` (`id_empleado`, `cedula_empleado`, `nombre_empleado`, `apellido_empleado`, `telf_empleado`, `codigo_empleado`, `email_empleado`, `fechai_empleado`, `id_cargo`, `id_departamento`, `tipo_empleado`, `fechac_empleado`, `fecham_empleado`) VALUES
(1, 14557401, 'Nelson', 'Valera', NULL, NULL, NULL, '2005-07-14', 50, 3, 'Empleado', '2013-06-07 00:00:00', '2013-07-24 17:59:43'),
(2, 15556778, 'Juan', 'Araujo', '234324', '234324', NULL, NULL, 1, 1, 'Empleado', '2013-07-17 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_parametros`
--

CREATE TABLE IF NOT EXISTS `tbl_parametros` (
  `id_parametro` int(11) NOT NULL AUTO_INCREMENT,
  `horasdiarias_parametro` decimal(4,2) NOT NULL DEFAULT '0.00',
  `cestaticketdia_parametro` decimal(4,2) NOT NULL DEFAULT '0.00',
  `sso_parametro` decimal(4,2) NOT NULL DEFAULT '0.00',
  `pf_parametro` decimal(4,2) NOT NULL DEFAULT '0.00',
  `fj_parametro` decimal(4,2) NOT NULL DEFAULT '0.00',
  `fvh_parametro` decimal(4,2) NOT NULL DEFAULT '0.00',
  `bnf_parametro` decimal(4,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id_parametro`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `tbl_parametros`
--

INSERT INTO `tbl_parametros` (`id_parametro`, `horasdiarias_parametro`, `cestaticketdia_parametro`, `sso_parametro`, `pf_parametro`, `fj_parametro`, `fvh_parametro`, `bnf_parametro`) VALUES
(1, '6.00', '1.00', '4.00', '0.50', '3.00', '1.00', '50.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuarios`
--

CREATE TABLE IF NOT EXISTS `tbl_usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_empleado` int(11) NOT NULL,
  `login_usuario` varchar(20) NOT NULL,
  `password_usuario` varchar(100) NOT NULL,
  `fechac_usuario` datetime NOT NULL,
  `fecham_usuario` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `id_empleado` (`id_empleado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id_usuario`, `id_empleado`, `login_usuario`, `password_usuario`, `fechac_usuario`, `fecham_usuario`) VALUES
(1, 1, 'nelson', '202cb962ac59075b964b07152d234b70', '2013-06-07 00:00:00', '2013-07-19 22:06:23');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
